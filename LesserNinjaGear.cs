using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace LesserNinjaGear {
	public class LesserNinjaGear : Mod {
		public LesserNinjaGear() {
			Properties = new ModProperties {
				Autoload = true,
				AutoloadGores = true,
				AutoloadSounds = true
			};
		}
	}

	public class NinjaGear1 : ModItem {
		public override void SetStaticDefaults() {
			DisplayName.SetDefault("Ninja Gear");
		}
		
		public override void SetDefaults() {
			item.width = 16;
			item.height = 24;
			item.accessory = true;
			item.rare = 7;
			item.value = 250000;
			item.handOnSlot = (sbyte) 11;
			item.handOffSlot = (sbyte) 6;
			item.waistSlot = (sbyte) 10;
		}
		
		public override void AddRecipes() {
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.BlackBelt, 1);
			recipe.AddIngredient(ItemID.ClimbingClaws, 1);
			recipe.AddTile(TileID.TinkerersWorkbench);
			recipe.SetResult(this, 1);
			recipe.AddRecipe();
			
			recipe = new ModRecipe(mod);
			recipe.AddIngredient(this, 1);
			recipe.AddIngredient(ItemID.ShoeSpikes);
			recipe.AddIngredient(ItemID.Tabi);
			recipe.AddTile(TileID.TinkerersWorkbench);
			recipe.SetResult(ItemID.MasterNinjaGear, 1);
			recipe.AddRecipe();
		}
		
		public override bool CanEquipAccessory (Player player, int slot) {
            for (int i = 3; i < 8 + player.extraAccessorySlots; i++)  {
				if (slot != i) {
					switch (player.armor[i].type) {
						case ItemID.BlackBelt:
						case ItemID.ClimbingClaws:
						case ItemID.MasterNinjaGear:
						case ItemID.TigerClimbingGear:
							return false;
					}
					if (player.armor[i].type == mod.ItemType<NinjaGear1>() || player.armor[i].type == mod.ItemType<NinjaGear2>()) {
						return false;
					}
				}
			}
			return true;
		}
		
		public override void ModifyTooltips (List<TooltipLine> tooltips) {
			tooltips.Add(new TooltipLine(mod, "Tooltip1", "Allows the ability to slide down walls"));
			tooltips.Add(new TooltipLine(mod, "Tooltip2", "Gives a chance to dodge attacks"));
		}
		
		public override void UpdateEquip (Player player) {
            player.blackBelt = true;
            player.spikedBoots = player.spikedBoots + 1;
		}
	}

	public class NinjaGear2 : ModItem {
		public override void SetStaticDefaults() {
			DisplayName.SetDefault("Ninja Gear");
		}
		
		public override void SetDefaults() {
			item.width = 16;
			item.height = 24;
			item.accessory = true;
			item.rare = 7;
			item.value = 250000;
			item.shoeSlot = (sbyte) 14;
			item.waistSlot = (sbyte) 10;
		}
		
		public override void AddRecipes() {
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.BlackBelt, 1);
			recipe.AddIngredient(ItemID.ShoeSpikes, 1);
			recipe.AddTile(TileID.TinkerersWorkbench);
			recipe.SetResult(this, 1);
			recipe.AddRecipe();
			
			recipe = new ModRecipe(mod);
			recipe.AddIngredient(this, 1);
			recipe.AddIngredient(ItemID.ClimbingClaws);
			recipe.AddIngredient(ItemID.Tabi);
			recipe.AddTile(TileID.TinkerersWorkbench);
			recipe.SetResult(ItemID.MasterNinjaGear, 1);
			recipe.AddRecipe();
		}
		
		public override bool CanEquipAccessory (Player player, int slot) {
            for (int i = 3; i < 8 + player.extraAccessorySlots; i++)  {
				if (slot != i) {
					switch (player.armor[i].type) {
						case ItemID.BlackBelt:
						case ItemID.MasterNinjaGear:
						case ItemID.ShoeSpikes:
						case ItemID.TigerClimbingGear:
							return false;
					}
					if (player.armor[i].type == mod.ItemType<NinjaGear1>() || player.armor[i].type == mod.ItemType<NinjaGear2>()) {
						return false;
					}
				}
			}
			return true;
		}
		
		public override void ModifyTooltips (List<TooltipLine> tooltips) {
			tooltips.Add(new TooltipLine(mod, "Tooltip1", "Allows the ability to slide down walls"));
			tooltips.Add(new TooltipLine(mod, "Tooltip2", "Gives a chance to dodge attacks"));
		}
		
		public override void UpdateEquip (Player player) {
            player.blackBelt = true;
            player.spikedBoots = player.spikedBoots + 1;
		}
	}
	
	public class GItem : GlobalItem {
		public override bool CanEquipAccessory (Item item, Player player, int slot) {
			for (int i = 3; i < 8 + player.extraAccessorySlots; i++)  {
				if (slot != i) {
					switch (item.type) {
						case ItemID.MasterNinjaGear:
						case ItemID.TigerClimbingGear:
							if (player.armor[i].type == mod.ItemType<NinjaGear1>() || player.armor[i].type == mod.ItemType<NinjaGear2>()) { return false; }
							break;
						case ItemID.ClimbingClaws:
							if (player.armor[i].type == mod.ItemType<NinjaGear1>()) { return false; }
							break;
						case ItemID.ShoeSpikes:
							if (player.armor[i].type == mod.ItemType<NinjaGear2>()) { return false; }
							break;
					}
				}
			}
			return true;
		}
	}
}